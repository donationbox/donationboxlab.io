## Information / Информация

Файлы автоматической установки GNU/Linux.

## Install / Установка

- [CentOS Server](centos.01.cfg)
- [Debian VM](debian.vm.cfg)
- [Debian Server](debian.01.cfg)
- [Fedora Server](fedora.01.cfg)
- [Fedora Workstation](fedora.02.cfg)
- [Oracle Server](oracle.01.cfg)
